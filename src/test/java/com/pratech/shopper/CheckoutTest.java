package com.pratech.shopper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.pratech.shopper.calculator.CostCalculator;
import com.pratech.shopper.calculator.DiscountCalculator;
import com.pratech.shopper.catalog.ProductNotFoundInCatalogException;

public class CheckoutTest {
	 private final Checkout checkoutWithoutDiscount = new Checkout(new CostCalculator());
	 private final Checkout checkoutWithDiscount = new Checkout(new CostCalculator(),new DiscountCalculator());
	
	@Rule
    public ExpectedException expectedException = ExpectedException.none();
	
	@Test(expected = IllegalArgumentException.class)
    public void should_throw_error_given_no_basket() throws Exception {
        checkoutWithoutDiscount.calculateAsPenny(null);
    }
	
	@Test
	public void given_empty_basket_should_return_zero() throws ProductNotFoundInCatalogException {
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{}),is(new BigDecimal("0")));
	}
	
	@Test
	public void given_undefined_product_should_throw_error_with_message() throws Exception {
		expectedException.expect(ProductNotFoundInCatalogException.class);
        expectedException.expectMessage("unknownItem");
        checkoutWithoutDiscount.calculateAsPenny(new String[]{"unknownItem"});
	}

	@Test
	public void given_basket_with_defined_item_should_calculate_correct() throws Exception {
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{"Apple"}),is(new BigDecimal("60")));
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{"Apple", "Orange"}),is(new BigDecimal("85")));
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{"Apple", "Apple","Orange","Orange","Apple","Orange"}),is(new BigDecimal("255")));
	}

	@Test
	public void given_product_names_has_spaces_should_calculate_correct() throws Exception {
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{" Apple", "Apple "," Orange "," Orange"," Orange "}),is(new BigDecimal("195")));
	}

	@Test
	public void given_products_is_not_case_sensitive_should_calculate_correct() throws Exception{
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{"ApPLe"}),is(new BigDecimal("60")));
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{"ApPLe", "ApplE","orange","OranGE","ORANGE"}),is(new BigDecimal("195")));
	}
	
	@Test
	public void given_basket_with_space_and_not_case_sensitive_item_should_calculate_correct() throws Exception{
		assertThat(checkoutWithoutDiscount.calculateAsPenny(new String[]{" ApplE", " APPle"," OraNge ","  Apple","OrAnge  "}),is(new BigDecimal("230")));
	}
	
	@Test
	public void given_basket_with_offer_should_apply_discount() throws Exception {
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple", "Apple"}),is(new BigDecimal("60")));
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Orange", "Orange","Orange"}),is(new BigDecimal("50")));
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple", "Apple","Orange","Orange","Orange"}),is(new BigDecimal("110")));
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple", "Apple","Orange","Orange","Apple","Orange"}),is(new BigDecimal("170")));
	}
	
	@Test
	public void given_products_under_threshold_with_offer_should_not_apply_discount() throws Exception {
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple"}),is(new BigDecimal("60")));
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple", "Orange"}),is(new BigDecimal("85")));
		assertThat(checkoutWithDiscount.calculateAsPenny(new String[]{"Apple", "Orange","Orange"}),is(new BigDecimal("110")));
	}

}
