package com.pratech.shopper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

import com.pratech.shopper.calculator.CostCalculator;
import com.pratech.shopper.calculator.DiscountCalculator;

public class CheckoutAcceptanceTest {
	private final Checkout checkoutWithoutDiscount = new Checkout(new CostCalculator());
	 private final Checkout checkoutWithDiscount = new Checkout(new CostCalculator(),new DiscountCalculator());

    @Test
    public void should_calculate_all_cost_correct_without_discount() throws Exception {
        assertThat(checkoutWithoutDiscount.calculateAsPound(new String[]{"Apple","Orange","Orange"}),
                is(new BigDecimal("1.10")));
        assertThat(checkoutWithoutDiscount.calculateAsPound(new String[]{"Apple","Apple","Orange","Orange","Orange"}),
                is(new BigDecimal("1.95")));
    }
    
    @Test
    public void should_support_basket_with_discount() throws Exception {
        assertThat(checkoutWithDiscount.calculateAsPound(new String[]{"Apple", "Apple","Orange","Orange","Orange"}),
                    is(new BigDecimal("1.10")));
        assertThat(checkoutWithDiscount.calculateAsPound(new String[]{"Apple", "Apple","Apple", "Apple",
        		"Orange","Orange","Orange","Orange","Orange"}),
                is(new BigDecimal("2.20")));
    }
    
}
