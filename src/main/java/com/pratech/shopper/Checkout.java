package com.pratech.shopper;

import java.math.BigDecimal;

import com.pratech.shopper.calculator.BasketCalculator;
import com.pratech.shopper.calculator.Calculator;
import com.pratech.shopper.catalog.ProductCatalog;
import com.pratech.shopper.catalog.ProductNotFoundInCatalogException;

public class Checkout {

	private final ProductCatalog catalog = new ProductCatalog();
    private final BasketCalculator basketCalculator;

    public Checkout(Calculator... calculators) {
    	basketCalculator = new BasketCalculator(calculators);
    }

    public BigDecimal calculateAsPound(String[] basket) throws ProductNotFoundInCatalogException {
        final BigDecimal totalAsPenny = calculateAsPenny(basket);
        return basketCalculator.convertPennyToPound(totalAsPenny);
    }

    public BigDecimal calculateAsPenny(String[] basket) throws ProductNotFoundInCatalogException {
        validateBasket(basket);
        long sumInPenny = basketCalculator.calculate(basket);
        return new BigDecimal(sumInPenny);
    }

    private void validateBasket(String[] basket) throws ProductNotFoundInCatalogException {

        if (basket == null ) {
            throw new IllegalArgumentException("Must be given a basket!");
        }

        for (String product : basket) {
            if (!catalog.containsItem(product)) {
                throw new ProductNotFoundInCatalogException(product);
            }
        }

    }

}
