package com.pratech.shopper.calculator;

import java.math.BigDecimal;

public class Converter {
    public static final BigDecimal PENNY_TO_POUND_DIVISOR = new BigDecimal("100");

    BigDecimal convertPennyToPound(BigDecimal totalAsPenny) {
        return totalAsPenny.divide(PENNY_TO_POUND_DIVISOR).setScale(2);
    }

}
