package com.pratech.shopper.calculator;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import com.pratech.shopper.catalog.Product;

public class DiscountCalculator extends Calculator {

    public long calculate(String[] basket) {

        final Map<Product, Long> groupedByItems = Arrays.stream(basket)
                .collect(Collectors.groupingBy(item -> catalog.getItemByName(item), Collectors.counting()));

        return groupedByItems.entrySet().stream()
                .mapToLong(entry -> (entry.getValue()/entry.getKey().getDiscountThreshold())*entry.getKey().getDiscount())
                .sum()* -1;
    }
}