package com.pratech.shopper.calculator;

import com.pratech.shopper.catalog.ProductCatalog;

public abstract class Calculator  {
    protected final ProductCatalog catalog = new ProductCatalog();
    public abstract long calculate(String[] basket);
}
