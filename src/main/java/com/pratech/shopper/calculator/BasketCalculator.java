package com.pratech.shopper.calculator;

import java.math.BigDecimal;
import java.util.Arrays;

public class BasketCalculator extends Calculator {

    private final Converter converter;
    private final Calculator[] calculators;

    public BasketCalculator(Calculator... calculators) {
        super();
        this.calculators = calculators;
        converter = new Converter();
    }

    public long calculate(String[] basket) {
        if (basket.length == 0) {
            return 0;
        }
        return Arrays.stream(calculators).mapToLong(calc -> calc.calculate(basket)).sum();
    }

    public BigDecimal convertPennyToPound(BigDecimal totalAsPenny) {
        return converter.convertPennyToPound(totalAsPenny);
    }

}