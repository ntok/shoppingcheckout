package com.pratech.shopper.calculator;

import java.math.BigDecimal;
import java.util.Arrays;

public class CostCalculator extends Calculator {
	private final Converter converter=new Converter();
	
    public long calculate(String[] basket) {
        final long sumInPenny = Arrays.stream(basket)
                .mapToLong(item -> catalog.getPriceOfItem(item)).sum();
        return sumInPenny;
    }
    
    public BigDecimal convertPennyToPound(BigDecimal totalAsPenny) {
        return converter.convertPennyToPound(totalAsPenny);
    }
}
