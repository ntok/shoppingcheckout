package com.pratech.shopper.catalog;

import java.util.Objects;

public class Product {

    private final String name;
    private final int price;
    private final int discountThreshold;
    private final int discount;

	public Product(String name, int price, int discountThreshold, int discount) {
		super();
		this.name = name;
		this.price = price;
		this.discountThreshold = discountThreshold;
		this.discount = discount;
	}

	public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product item = (Product) o;
        return Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }

	/**
	 * @return the discountThreshold
	 */
	public int getDiscountThreshold() {
		return discountThreshold;
	}

	/**
	 * @return the discount
	 */
	public int getDiscount() {
		return discount;
	}
    
    
}
