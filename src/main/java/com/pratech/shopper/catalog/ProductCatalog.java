package com.pratech.shopper.catalog;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductCatalog {

    public static final Product[] catalogItems =  {new Product("Apple", 60,2,60), 
                                                new Product("Orange",25,3,25)   };

    private static final Map<String, Product> catalogItemsByName = Arrays.stream(catalogItems)
                                                            .collect(Collectors.toMap(item -> item.getName().toLowerCase(), Function.identity()));

    public ProductCatalog() {
    }

    public boolean containsItem(String item) {
        return catalogItemsByName.containsKey(item.trim().toLowerCase());
    }

    public int getPriceOfItem(String item) {
        return getItemByName(item).getPrice();
    }

    public Product getItemByName(String item) {
        return catalogItemsByName.get(item.trim().toLowerCase());
    }


}
