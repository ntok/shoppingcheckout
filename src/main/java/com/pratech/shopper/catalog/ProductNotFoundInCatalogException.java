package com.pratech.shopper.catalog;

public class ProductNotFoundInCatalogException extends Exception {
    public ProductNotFoundInCatalogException(String message) {
        super(message);
    }
}
